# Statham language
Statham is a programming language developed as a hobby. It is a mix of all sorts of features that I found interesting, but the main syntax is essentially a mix of Java and C.


To begin with, Statham is an interpreted language, but making a compiler for it is next on my list.

## Language features
* Expressions and statements just like in Java
* Structs just like in C
* References (pointers) just like in C
* Method calls (although they can't be defined)
* Function definitions
* Auto-wrapping for-each element into pointers if desired
* Auto-unwrapping one level of pointers during struct and method accesses.
