aspect ReferenceAnalysis {
    inh boolean VarDecl.isForEachElement();
    eq ForEachStmt.getElement().isForEachElement() = true;
    eq Program.getChild(int index).isForEachElement() = false;

    syn boolean VarDecl.hasCorrectForEachInit();
    eq VarDecl.hasCorrectForEachInit() = !isForEachElement() || !getDeclarator(0).hasInit();

    syn boolean DereferenceExpr.hasCorrectOperand() = getOperand().type() instanceof RefType;
    syn boolean AddrOfExpr.hasCorrectOperand() = getOperand().getCleanAccess() instanceof CleanAccess;

    VarDecl contributes error("The for each loop element may not have an initializer!")
        when !hasCorrectForEachInit()
        to Program.errors() for program();

    DereferenceExpr contributes error("Dereferencing of a non-reference value!")
        when !hasCorrectOperand()
        to Program.errors() for program();

    AddrOfExpr contributes error("The address-of operator may only be used with variable and array accesses!")
        when !hasCorrectOperand()
        to Program.errors() for program();

    DereferenceExpr contributes error("Dereferencing of void references is not allowed!")
        when hasCorrectOperand() && ((RefType) getOperand().type()).getTargetType() == anyType()
        to Program.errors() for program();
}