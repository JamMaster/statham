aspect AssignAnalysis {
    /**
     * Used to check if the LHS of an assignment either an IdUse or an ArrayAccess
     */
    syn CleanAccess Expr.getCleanAccess();
    eq CleanAccess.getCleanAccess() = this;
    eq Expr.getCleanAccess() = null; //Default
    eq ParExpr.getCleanAccess() = getExpr().getCleanAccess();

    syn Access IncDecExpr.getVarAccess() = getOperand().getCleanAccess();

    AssignExpr contributes error("Left-hand side of the assignment must be a variable or an array access")
        when getDest().getCleanAccess() == null
        to Program.errors() for program();

    IncDecExpr contributes error("Invalid operator argument")
        when getVarAccess() == null
        to Program.errors() for program();
}