import java.util.stream.Stream;

aspect MultipleDecl {
    syn boolean FuncDecl.clashesPredefined() {
        String funcName = getName();
        if (funcName != null) {
            return predefinedFunctions().stream()
                    .flatMap(vd -> vd.getDeclarators().stream())
                    .map(VariableDeclarator::getIdDecl)
                    .map(IdDecl::getID)
                    .anyMatch(funcName::equals);
        } else {
            return false;
        }
    }

    inh boolean StructDecl.isMultiDeclared();

    eq Program.getDecl(int index).isMultiDeclared() {
        for (int i = 0; i < index; i++) {
            Decl d = getDecl(i);
            if (getDecl(index).equals(d)) {
                return true;
            }
        }

        return false;
    }
    eq Program.unknownStructDecl().isMultiDeclared() = false;

    FuncDecl contributes error("Function '" + getName() + "' is predefined, use different name")
        when clashesPredefined()
        to Program.errors() for program();

    StructDecl contributes error("Struct '" + getID() + "' is declared multiple times")
        when isMultiDeclared()
        to Program.errors() for program();
}