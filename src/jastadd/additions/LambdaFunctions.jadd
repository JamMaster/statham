import beaver.Symbol;

aspect LambdaFunctions {
    public FuncDeclExpr.FuncDeclExpr(TypeAccess returnType, List<ParameterDeclaration> paramDecls,
        Block bodyBlock) {
        this(new FuncDecl(returnType, paramDecls, bodyBlock));
    }

    //Convenience constructor for func decls
    public ConstVarDecl.ConstVarDecl(IdDecl funcName, TypeAccess returnTypeAccess, List<ParameterDeclaration> paramDecls, Block funcBody, int start) {
        this((IdDecl) funcName.setStart(start), new FuncDecl(returnTypeAccess, paramDecls, funcBody).setStart(start));
    }

    //Convenience constructor for predefined funcs
    public ConstVarDecl.ConstVarDecl(IdDecl funcName, FuncDecl decl) {
        this(new FuncTypeAccess("func", decl.getReturnTypeAccess(), decl.getParameters().stream()
                .map(ParameterDeclaration::getTypeAccess)
                .collect(List.toList())),
            new List<>(new VariableDeclarator(funcName, new Opt(new FuncDeclExpr(decl)))));
    }

    public static final String ParameterDeclaration.DEFAULT_PARAM_NAME = "param";

    public static List<ParameterDeclaration> ParameterDeclaration.fromTypes(List<TypeAccess> typeAccesses) {
        List<ParameterDeclaration> params = new List<>();

        for (int i = 0; i < typeAccesses.getNumChild(); i++) {
            params.add(new ParameterDeclaration(typeAccesses.getChild(i), new IdDecl(DEFAULT_PARAM_NAME + i)));
        }

        return params;
    }
}