aspect CastEvaluation {
    syn InnerType CastExpr.eval(StackFrame sf) {
        if (getTypeAccess() instanceof RefTypeAccess) {
            RefType refType = (RefType) getTypeAccess().type();

            if (getExpr().type() == intType()){
                throw new UnsupportedOperationException("Casting of addresses has no meaning during interpretation!");
            } else if (getExpr().type() instanceof RefType) {
                return getExpr().eval(sf); //We dont need to do anything special
            } else {
                throw new UnsupportedOperationException("Evaluation of casting from " + getExpr().type().getTypeName() +
                        " to " + getTypeAccess().type().getTypeName() + " undefined");
            }
        } else {
            Type exprType = getExpr().type();
            Object rawValue = getExpr().eval(sf).getValue();
            switch (getTypeAccess().getTypeName()) {
                case "int":
                    if (exprType == doubleType()) {
                        return InnerType.instantiate(((Double) rawValue).intValue());
                    } else if (exprType == booleanType()) {
                        return InnerType.instantiate(((Boolean) rawValue) ? 1 : 0);
                    }
                    break;
                case "double":
                    if (exprType == intType()) {
                        return InnerType.instantiate(((Integer) rawValue).doubleValue());
                    }
                    break;
                case "boolean":
                    if (exprType == intType()) {
                        return InnerType.instantiate(((Integer) rawValue) > 0);
                    }
                    break;
            }

            throw new UnsupportedOperationException("Evaluation of casting from " + exprType.getTypeName() + " to " +
                    getTypeAccess().type().getTypeName() + " undefined");
        }
    }
}