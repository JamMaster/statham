import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.Collectors;

aspect GeneratedMethods {
    syn List<MethodDecl> Type.getGeneratedMethodList();

    /**
     * Filter methods from clazz that are/have: non-public, native, non-conforming parameters/return
     * types
     */
    public static Stream<Method> FuncDecl.getCompatibleFunctions(Class<?> clazz, TypeAccess undetermined) {
        return Stream.of(clazz.getDeclaredMethods())
            .filter(m -> Modifier.isPublic(m.getModifiers()))
            .filter(m -> !Modifier.isNative(m.getModifiers()))
            .filter(m -> Type.getTypeAccess(m.getReturnType(), undetermined) != null)
            .filter(m -> Stream.of(m.getParameterTypes())
                .map(p -> Type.getTypeAccess(p, undetermined))
                .allMatch(Objects::nonNull));
    }

    public static Stream<Method> FuncDecl.filterByNames(Stream<Method> methods, String[] names) {
        return methods
            .filter(m -> Arrays.asList(names).contains(m.getName()));
    }

    public static Stream<Method> MethodDecl.filterStaticDuplicates(Stream<Method> methods) {
        return methods
            .filter(m -> !Modifier.isStatic(m.getModifiers())
                || (m.getParameterCount() == 1 && Stream.of(m.getParameterTypes())
                    .allMatch(p -> Type.wrap(p).equals(m.getDeclaringClass())) //Can be called with itself
                && !MethodDecl.hasNonStaticEquivalent(m))); //And doesn't have a non-static equivalent
    }

    public static boolean MethodDecl.hasNonStaticEquivalent(Method method) {
        return Stream.of(method.getDeclaringClass().getDeclaredMethods())
            .filter(m -> m.getParameterCount() == 0)
            .filter(m -> !Modifier.isStatic(m.getModifiers()))
            .map(m -> m.getName())
            .anyMatch(method.getName()::equals);
    }

    public static Stream<Method> MethodDecl.getMethods(String[] names, Class<?> clazz) {
        return MethodDecl.filterStaticDuplicates(
            FuncDecl.filterByNames(
                FuncDecl.getCompatibleFunctions(clazz, null), names));
    }

    public static <T> Class<T> Type.wrap(Class<T> c) {
        Map<Class<?>, Class<?>> PRIMITIVES_TO_WRAPPERS = new HashMap<>();
        PRIMITIVES_TO_WRAPPERS.put(boolean.class, Boolean.class);
        PRIMITIVES_TO_WRAPPERS.put(byte.class, Byte.class);
        PRIMITIVES_TO_WRAPPERS.put(char.class, Character.class);
        PRIMITIVES_TO_WRAPPERS.put(double.class, Double.class);
        PRIMITIVES_TO_WRAPPERS.put(float.class, Float.class);
        PRIMITIVES_TO_WRAPPERS.put(int.class, Integer.class);
        PRIMITIVES_TO_WRAPPERS.put(long.class, Long.class);
        PRIMITIVES_TO_WRAPPERS.put(short.class, Short.class);
        PRIMITIVES_TO_WRAPPERS.put(void.class, Void.class);

        return c.isPrimitive() ? (Class<T>) PRIMITIVES_TO_WRAPPERS.get(c) : c;
    }

    public static TypeAccess Type.getTypeAccess(Class<?> clazz, TypeAccess undetermined) {
        if (clazz.equals(String.class) || clazz.equals(CharSequence.class)) {
            return new TypeAccess("string");
        } else if (wrap(clazz).equals(Integer.class)) {
            return new TypeAccess("int");
        } else if (wrap(clazz).equals(Double.class)) {
            return new TypeAccess("double");
        } else if (wrap(clazz).equals(Boolean.class)) {
            return new TypeAccess("boolean");
        } else if (wrap(clazz).equals(Character.class)) {
            return new TypeAccess("char");
        } else if (wrap(clazz).equals(Void.class)) {
            return new TypeAccess("void");
        } else if (clazz.isArray()) {
            //Note, this is not an error, Class.isArray is used on purpose
            return new ArrayTypeAccess("array", getTypeAccess(clazz.getComponentType(), null));
        } else if (Collection.class.isAssignableFrom(clazz)) {
            return undetermined != null ? new ArrayTypeAccess("array", undetermined) : null;
        } else if (clazz.equals(Object.class)) {
            return undetermined != null ? undetermined : null;
        } else {
            return null;
        }
    }

    public static List<MethodDecl> MethodDecl.generateMethodDecls(Class<?> clazz, String[] names, int count) {
        java.util.List<Method> methods = MethodDecl.getMethods(names, clazz)
            .collect(Collectors.toList());
        List<MethodDecl> methodDecls = new List<>();

//        methods.forEach(m -> System.out.println(m));
//        System.out.println("");
        for (Method m : methods) {
            TypeAccess returnType = Type.getTypeAccess(m.getReturnType(), null);
            List<TypeAccess> paramTypes = new List<>();
            for (Class<?> c : m.getParameterTypes()) {
                paramTypes.add(Type.getTypeAccess(c, null));
            }
            String name = m.getName();

            List<ParameterDeclaration> params = new List<>();
            if (!Modifier.isStatic(m.getModifiers())) {
                params = ParameterDeclaration.fromTypes(paramTypes);
            }

            methodDecls.add(new MethodDecl(name, new GeneratedFuncDecl(returnType, params,
                null, m, Modifier.isStatic(m.getModifiers()))));
        }

        if (methodDecls.getNumChild() != count) {
            throw new CompilerError(
                new IllegalStateException(clazz.getSimpleName() + " has generated "
                    + methodDecls.getNumChild() + " instead of " + count + " methods!"));
        }

        return methodDecls;
    }

    public static List<MethodDecl> MethodDecl.generateArrayMethodDecls(Stream<Method> methodStream,
        int count, TypeAccess elementType) {
        java.util.List<Method> methods = methodStream.collect(Collectors.toList());
        List<MethodDecl> methodDecls = new List<>();

        for (Method m : methods) {
            methodDecls.add(generateMethodDeclFromMethod(m, elementType));
        }

        if (methodDecls.getNumChild() != count) {
            throw new CompilerError(
                new IllegalStateException("ArrayList has generated "
                    + methodDecls.getNumChild() + " instead of " + count + " methods!"));
        }

        return methodDecls;
    }

    private static MethodDecl MethodDecl.generateMethodDeclFromMethod(Method m, TypeAccess elementType) {
        TypeAccess returnType = Type.getTypeAccess(m.getReturnType(), elementType);
        List<TypeAccess> paramTypes = new List<>();
        for (Class<?> c : m.getParameterTypes()) {
            paramTypes.add(Type.getTypeAccess(c, elementType));
        }

        List<ParameterDeclaration> params = ParameterDeclaration.fromTypes(paramTypes);

        MethodDecl md = new MethodDecl(m.getName(), new GeneratedFuncDecl(returnType, params,
            null, m, Modifier.isStatic(m.getModifiers())));

        return md;
    }

    eq StringType.getGeneratedMethodList() {
        final String[] METHODS = new String[] {
            "hashCode", "indexOf", "length", "isEmpty", "charAt", "codePointAt",
            "codePointBefore", "codePointCount", "offsetByCodePoints", "equalsIgnoreCase",
            "regionMatches", "startsWith", "endsWith", "lastIndexOf", "substring",
            "replace", "matches", "contains", "replaceFirst", "replaceAll", "split",
            "toLowerCase", "toUpperCase", "trim", "toCharArray", "compareTo"
        };
        final int COUNT = 37;

        return MethodDecl.generateMethodDecls(String.class, METHODS, COUNT);
    }

    /**
     * There is no 1:1 correspondence, so its done by hand
     */
    eq ArrayType.getGeneratedMethodList() {
//        .forEach(m -> System.out.print("\"" + m.getName() + "\", "));
//        Arrays.asList(ArrayList.class.getDeclaredMethods()).forEach(System.out::println);
        final String[] METHODS = new String[] {
            "get", "indexOf", "isEmpty", "lastIndexOf",
            "contains", "size", "set"
        };
        final int COUNT = 7;

        Stream<Method> methods = FuncDecl.filterByNames(FuncDecl
            .getCompatibleFunctions(ArrayList.class, getBaseType().getTypeAccess()), METHODS);

        return MethodDecl.generateArrayMethodDecls(methods, COUNT,
            getBaseType().getTypeAccess());
    }

    eq BooleanType.getGeneratedMethodList() {
        final String[] METHODS = new String[] {
            "toString", "hashCode", "compareTo"
        };
        final int COUNT = 3;

        return MethodDecl.generateMethodDecls(Boolean.class, METHODS, COUNT);
    }

    eq IntType.getGeneratedMethodList() {
        final String[] METHODS = new String[] {
            "numberOfLeadingZeros", "numberOfTrailingZeros", "bitCount", "toString",
            "hashCode", "compareTo", "doubleValue", "toHexString", "reverse",
            "toUnsignedString", "toOctalString", "toBinaryString", "highestOneBit",
            "lowestOneBit", "signum"
        };
        final int COUNT = 15;

        return MethodDecl.generateMethodDecls(Integer.class, METHODS, COUNT);
    }

    eq CharType.getGeneratedMethodList() {
        final String[] METHODS = new String[] {
            "toString", "hashCode", "compareTo", "toLowerCase", "toUpperCase",
            "isDigit", "isSurrogate", "isLowSurrogate", "isHighSurrogate",
            "isLetter", "isLetterOrDigit", "isLowerCase", "isUpperCase",
            "isTitleCase", "isDefined", "toTitleCase", "getNumericValue",
            "isSpaceChar", "isWhitespace", "isISOControl", "isMirrored"
        };
        final int COUNT = 21;

        return MethodDecl.generateMethodDecls(Character.class, METHODS, COUNT);
    }

    eq DoubleType.getGeneratedMethodList() {
        final String[] METHODS = new String[] {
            "toString", "hashCode", "compareTo", "intValue", "toHexString",
            "isNaN", "isInfinite", "isFinite"
        };
        final int COUNT = 8;

        return MethodDecl.generateMethodDecls(Double.class, METHODS, COUNT);
    }

    eq JsonObjType.getGeneratedMethodList() = new List<MethodDecl>();
    eq JsonElemType.getGeneratedMethodList() = new List<MethodDecl>();

    eq AnyType.getGeneratedMethodList() = null;
    eq VoidType.getGeneratedMethodList() = null;
    eq UnknownType.getGeneratedMethodList() = null;
    eq RefType.getGeneratedMethodList() = null; //Refs cant have methods because of auto-dereferencing!
    eq StructType.getGeneratedMethodList() = new List<MethodDecl>();
    eq FuncType.getGeneratedMethodList() = new List<MethodDecl>();
}