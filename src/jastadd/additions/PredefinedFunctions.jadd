aspect PredefinedFunctions {
    syn nta List<VarDecl> Program.predefinedFunctions() {
        List<VarDecl> list = new List<>();
        // Create objects of type FuncDecl and add them to the list
        list.add(new ConstVarDecl(new IdDecl("itos"),
            new PredefinedFuncDecl(new TypeAccess("string"),
            new List().add(new ParameterDeclaration(new TypeAccess("int"), new IdDecl("i"))),
            null, "itos")));
        list.add(new ConstVarDecl(new IdDecl("dtos"),
            new PredefinedFuncDecl(new TypeAccess("string"),
            new List().add(new ParameterDeclaration(new TypeAccess("double"), new IdDecl("d"))),
            null, "dtos")));
        list.add(new ConstVarDecl(new IdDecl("stoi"),
            new PredefinedFuncDecl(new TypeAccess("int"),
            new List().add(new ParameterDeclaration(new TypeAccess("string"), new IdDecl("s"))),
            null, "stoi")));
        list.add(new ConstVarDecl(new IdDecl("randInt"),
            new PredefinedFuncDecl(new TypeAccess("int"),
            new List(),
            null, "randInt")));
        list.add(new ConstVarDecl(new IdDecl("randInt"),
            new PredefinedFuncDecl(new TypeAccess("int"),
            new List().add(new ParameterDeclaration(new TypeAccess("int"), new IdDecl("bound"))),
            null, "randInt")));
        list.add(new ConstVarDecl(new IdDecl("randDouble"),
            new PredefinedFuncDecl(new TypeAccess("double"),
            new List(),
            null, "randDouble")));

        //Overloaded print function for all primitive types
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("string"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("int"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("double"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("boolean"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("char"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("jsonObj"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("jsonElem"), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new ArrayTypeAccess("array", new TypeAccess("any")), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new RefTypeAccess("ref", new TypeAccess("any")), new IdDecl("param"))),
            null, "print")));
        list.add(new ConstVarDecl(new IdDecl("print"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new StructTypeAccess("0any"), new IdDecl("param"))),
            null, "print"))); //normal identifiers cannot begin with a number

        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("string"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("int"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("double"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("boolean"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("char"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("jsonObj"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new TypeAccess("jsonElem"), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new ArrayTypeAccess("array", new TypeAccess("any")), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new RefTypeAccess("ref", new TypeAccess("any")), new IdDecl("param"))),
            null, "print_debug")));
        list.add(new ConstVarDecl(new IdDecl("print_debug"),
            new PredefinedFuncDecl(new TypeAccess("void"),
            new List().add(new ParameterDeclaration(new StructTypeAccess("0any"), new IdDecl("param"))),
            null, "print_debug"))); //normal identifiers cannot begin with a number

        list.addAll(getGeneratedFunctionList());
        list.forEach(vd -> vd.setParent(this));

        return list;
    }

    //Make the predefined functions visible in all children node by broadcasting
    inh List<VarDecl> ASTNode.predefinedFunctions();
    eq Program.getChild().predefinedFunctions() = predefinedFunctions();

    syn boolean FuncDecl.isPredefined() = false;
    eq PredefinedFuncDecl.isPredefined() = true;
    eq GeneratedFuncDecl.isPredefined() = true;

    syn boolean FuncDecl.isGenerated() = false;
    eq GeneratedFuncDecl.isGenerated() = true;

    syn boolean VarDecl.isConstantDecl() = false;
    eq ConstVarDecl.isConstantDecl() = true;

    inh boolean IdDecl.isConstantIdDecl();
    eq VarDecl.getDeclarator().isConstantIdDecl() = false;
    eq ConstVarDecl.getDeclarator().isConstantIdDecl() = true;
    eq FieldDecl.getVarDecl().isConstantIdDecl() = false; //Field var decls cant be constant decls
    eq ParameterDeclaration.getIdDecl().isConstantIdDecl() = false; //Param decls cant be constant decls
    eq Program.unknownVarDecl().isConstantIdDecl() = false;
}