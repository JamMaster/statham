Program ::= Decl*;

abstract Decl;

TypeAccess ::= <TypeName:String>;
ArrayTypeAccess : TypeAccess ::= TypeAccess;
RefTypeAccess : TypeAccess ::= TypeAccess;
StructTypeAccess : TypeAccess;
FuncTypeAccess : TypeAccess ::= ReturnTypeAccess:TypeAccess ParamTypeAccess:TypeAccess*;
abstract Type ::= <ID:String> /PredefinedMethod:MethodDecl*/;

VoidType : Type;
UnknownType : Type;
abstract NumericType : Type;
BooleanType : Type;
StringType : Type;
IntType : NumericType;
CharType : Type;
DoubleType : NumericType;
RefType : Type ::= TargetType:Type;
ArrayType : Type ::= BaseType:Type;
StructType : Type;
FuncType : Type ::= FuncDecl;
AnyType : Type;

abstract JsonType : Type;
JsonObjType : JsonType;
JsonElemType : JsonType;

IdDecl ::= <ID:String>;

VariableDeclarator ::= IdDecl [Init:Expr];
ParameterDeclaration ::= TypeAccess IdDecl;
FieldDecl ::= /StructDecl/ VarDecl;
VarDecl : Decl ::= TypeAccess Declarator:VariableDeclarator*;
ConstVarDecl : VarDecl;
FuncDecl ::= ReturnTypeAccess:TypeAccess Parameter:ParameterDeclaration* Body:Block;
StructDecl : Decl ::= <ID:String> FieldDecl*;
PredefinedFuncDecl : FuncDecl ::= <ID:String>;
GeneratedFuncDecl : FuncDecl ::= <Method:Method> <IsStatic:boolean>;
MethodDecl ::= <ID:String> FuncDecl;

UnknownFuncDecl : FuncDecl;
UnknownVarDecl : IdDecl;
UnknownStructDecl : StructDecl;

abstract Access : Expr;
abstract CleanAccess : Access;

FuncAccess : Access ::= Expr Arg:Expr*;
AllocInvocation : Access ::= TypeAccess [Arg:Expr];
FreeInvocation : Access ::= Arg:Expr;
MethodAccess : FuncAccess ::= Left:Expr;
IdUse : CleanAccess ::= <ID:String>;
ArrayAccess : CleanAccess ::= Expr Index:Expr;
DereferenceExpr : CleanAccess ::= Operand:Expr;
StructFieldAccess : CleanAccess ::= Left:Expr Field:IdUse;

abstract Expr;

StructFieldAssign ::= <ID:String> Source:Expr;
abstract AssignExpr : Expr ::= Dest:Expr Source:Expr;

AssignSimpleExpr : AssignExpr;

abstract AssignArithmeticExpr : AssignExpr;

abstract AssignMultiplicativeExpr : AssignArithmeticExpr;
AssignMulExpr : AssignMultiplicativeExpr;
AssignDivExpr : AssignMultiplicativeExpr;
AssignModExpr : AssignMultiplicativeExpr;

abstract AssignAdditiveExpr : AssignArithmeticExpr;
AssignPlusExpr : AssignAdditiveExpr;
AssignMinusExpr : AssignAdditiveExpr;

abstract AssignShiftExpr : AssignExpr;
AssignLShiftExpr : AssignShiftExpr;
AssignRShiftExpr : AssignShiftExpr;
AssignURShiftExpr : AssignShiftExpr;

abstract AssignBitwiseExpr : AssignExpr;
AssignAndExpr : AssignBitwiseExpr;
AssignXorExpr : AssignBitwiseExpr;
AssignOrExpr : AssignBitwiseExpr;

abstract PrimaryExpr : Expr;

ParExpr : PrimaryExpr ::= Expr;

abstract Unary : Expr ::= Operand:Expr;
MinusExpr : Unary;
PlusExpr : Unary;
BitNotExpr : Unary;
LogicalNotExpr : Unary;

AddrOfExpr : Unary;

abstract IncDecExpr : Unary;
PreIncExpr : IncDecExpr;
PreDecExpr : IncDecExpr;

abstract PostfixExpr : IncDecExpr;
PostIncExpr : PostfixExpr;
PostDecExpr : PostfixExpr;

CastExpr : Expr ::= TypeAccess Expr;

abstract Binary : Expr ::= LeftOperand:Expr RightOperand:Expr;

abstract ArithmeticExpr : Binary;
ExpExpr : ArithmeticExpr;
abstract MultiplicativeExpr : ArithmeticExpr;
MulExpr : MultiplicativeExpr;
DivExpr : MultiplicativeExpr;
ModExpr : MultiplicativeExpr;
abstract AdditiveExpr : ArithmeticExpr;
AddExpr : AdditiveExpr;
SubExpr : AdditiveExpr;

abstract ShiftExpr : Binary;
LShiftExpr : ShiftExpr;
RShiftExpr : ShiftExpr;
URShiftExpr : ShiftExpr;

abstract BitwiseExpr : Binary;
AndBitwiseExpr : BitwiseExpr;
OrBitwiseExpr : BitwiseExpr;
XorBitwiseExpr : BitwiseExpr;

abstract LogicalExpr : Binary;
AndLogicalExpr : LogicalExpr;
OrLogicalExpr : LogicalExpr;

abstract RelationalExpr : Binary;
LTExpr : RelationalExpr;
GTExpr : RelationalExpr;
LEExpr : RelationalExpr;
GEExpr : RelationalExpr;

abstract EqualityExpr : RelationalExpr;
EQExpr : EqualityExpr;
NEExpr : EQExpr;

ConditionalExpr : Expr ::= Condition:Expr TrueExpr:Expr FalseExpr:Expr;

abstract Stmt;

abstract Loop : Stmt;
abstract ConditionalLoop : Loop ::= Condition:Expr;

Block : Stmt ::= Stmt*;
EmptyStmt : Stmt;
ExprStmt : Stmt ::= Expr;

VarDeclStmt : Stmt ::= VarDecl;
IfStmt : Stmt ::= Condition:Expr Then:Block [Else:Stmt];
WhileStmt : ConditionalLoop ::= Block;
DoStmt : ConditionalLoop ::= Block;
ForStmt : ConditionalLoop ::= InitStmt:Stmt* Condition:Expr UpdateStmt:Stmt* Block;
ForEachStmt : Loop ::= Element:VarDecl Collection:Expr Block;

abstract FlowControlStmt : Stmt;
abstract LoopControlStmt : FlowControlStmt;
BreakStmt : LoopControlStmt;
ContinueStmt : LoopControlStmt;
ReturnStmt : FlowControlStmt ::= [Result:Expr];

abstract Literal : PrimaryExpr ::= <Literal:String>;
BooleanLiteral : Literal;
CharacterLiteral : Literal;
StringLiteral : Literal;
IntegerLiteral : Literal ::= <IsHex:boolean>;
DoubleLiteral : Literal;
NullLiteral : Literal;
ArrayCreationExpr : PrimaryExpr ::= ArrayTypeAccess Dim:Expr* Init:Expr*;
StructLiteral : PrimaryExpr ::= StructTypeAccess Init:StructFieldAssign*;
FuncDeclExpr : PrimaryExpr ::= FuncDecl;