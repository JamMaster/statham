package statham;

import org.junit.runners.Parameterized;
import statham.ast.LangParser;
import statham.ast.model.CompilerError;
import statham.ast.model.ErrorMessage;
import statham.ast.model.Program;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.junit.Assert.fail;

public abstract class TestBase {
    protected static File TEST_DIRECTORY;
    protected static String EXTENSION;

    @Parameterized.Parameter(0)
    public String name;

    @Parameterized.Parameter(1)
    public String filePath;

    protected void doTest(boolean failOnCompileError) throws Exception{
        PrintStream err = System.err;
        PrintStream out = System.out;

        ByteArrayOutputStream stderr = new ByteArrayOutputStream();
        ByteArrayOutputStream stdout = new ByteArrayOutputStream();

        PrintStream errPrintStream = new PrintStream(stderr);
        PrintStream printStream = new PrintStream(stdout);
        try {
            System.setErr(errPrintStream);
            System.setOut(printStream);

            Program program = (Program) TestUtil.parse(new File(filePath));

            if (program.errors().isEmpty()) {
                onNoCompilationErrors(program, printStream, stdout);
            } else {
                onCompilationErrors(program, stdout, stderr, failOnCompileError);
            }
        } catch (LangParser.SyntaxError e) {
            Files.deleteIfExists(new File(TestUtil.changeExtension(filePath, ".out")).toPath());
            Files.write(new File(TestUtil.changeExtension(filePath, ".out")).toPath(), stderr.toString().getBytes(),
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            writeStreams(stderr, stdout);

            System.err.println(e.getMessage());
            fail(e.getMessage());
        } catch (Exception e2) {
            Files.deleteIfExists(new File(TestUtil.changeExtension(filePath, ".out")).toPath());
            writeStreams(stderr, stdout);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            if (e2 instanceof CompilerError) {
                ((CompilerError) e2).printRealStackTrace(pw);
            } else {
                pw.println(e2.getMessage());
                e2.printStackTrace(pw);
            }
            String error = sw.toString();

            Files.write(new File(TestUtil.changeExtension(filePath, ".out")).toPath(), error.getBytes(),
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

            fail(e2.getMessage());
        } finally {
            System.setErr(err);
            System.setOut(out);
        }
    }

    protected void onCompilationErrors(Program program, ByteArrayOutputStream stdout,
                                       ByteArrayOutputStream stderr, boolean failOnCompileError) throws IOException {
        String actual = program.errors().stream()
                .map(ErrorMessage::toString)
                .collect(Collectors.joining("\n"));

        Files.deleteIfExists(new File(TestUtil.changeExtension(filePath, ".out")).toPath());
        Files.write(new File(TestUtil.changeExtension(filePath, ".out")).toPath(), actual.getBytes(),
                StandardOpenOption.APPEND, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        writeStreams(stderr, stdout);

        TestUtil.compareWithExpected(actual,
                new File(TestUtil.changeExtension(filePath, ".expected")));
    }

    protected void onNoCompilationErrors(Program program, PrintStream printStream, ByteArrayOutputStream stdout) {
        program.execute("testFunction", new ArrayList<>(), printStream);

        TestUtil.compareOutput(stdout.toString(),
                new File(TestUtil.changeExtension(filePath, ".out")),
                new File(TestUtil.changeExtension(filePath, ".expected")));
    }

    private void writeStreams(ByteArrayOutputStream stderr, ByteArrayOutputStream stdout) throws IOException {
        if (stdout != null) {
            Files.write(new File(TestUtil.changeExtension(filePath, ".out")).toPath(), stdout.toString().getBytes(),
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        }
        if (stderr != null) {
            Files.write(new File(TestUtil.changeExtension(filePath, ".out")).toPath(), stderr.toString().getBytes(),
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        }
    }
}
