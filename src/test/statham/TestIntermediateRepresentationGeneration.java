package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import statham.ast.model.Program;
import statham.ast.representation.intermediate.value.IntermediateRepresentation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

@RunWith(Parameterized.class)
public class TestIntermediateRepresentationGeneration extends TestBase {
    static {
        TEST_DIRECTORY = new File("testfiles/generation/intermediate");
        EXTENSION = ".stm";
    }

    @Test
    public void assertIntermediateCodeGeneration() throws Exception {
        doTest(true);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY, EXTENSION);
    }

    @Override
    protected void onNoCompilationErrors(Program program, PrintStream printStream, ByteArrayOutputStream stdout) {
        IntermediateRepresentation ir = program.generateIntermediate();

        TestUtil.compareOutput(ir.toString(),
                new File(TestUtil.changeExtension(filePath, ".out")),
                new File(TestUtil.changeExtension(filePath, ".expected")));
    }
}
