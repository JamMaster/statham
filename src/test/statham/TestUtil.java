package statham;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;

import statham.ast.LangParser;
import statham.ast.LangScanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestUtil {
    private static String SYS_LINE_SEP = System.getProperty("line.separator");

    public static Object parse(File file) throws Exception {
        LangScanner scanner = new LangScanner(new FileReader(file));
        LangParser parser = new LangParser();
        return parser.parse(scanner);
    }

    public static void compareOutput(String actual, File out, File expected) {
        try {
            Files.write(out.toPath(), actual.getBytes());
        } catch (IOException e) {
            System.out.println("Could not write to output file!");
        }
        compareWithExpected(actual, expected);
    }

    public static void compareWithExpected(String actual, File expected) {
        try {
            assertEquals("Output differs",
                    readFileToString(expected),
                    normalizeText(actual));
        } catch (FileNotFoundException e) {
            fail("IOException occurred while comparing output: " + e.getMessage());
        }
    }

    private static String readFileToString(File file) throws FileNotFoundException {
        if (!file.isFile()) {
            return "";
        }

        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("\\Z");
        String text = normalizeText(scanner.hasNext() ? scanner.next() : "");
        scanner.close();
        return text;
    }

    private static String normalizeText(String text) {
        return text.replace(SYS_LINE_SEP, "\n").trim();
    }

    public static void testValidSyntax(File directory, String filename) {
        try {
            parse(new File(directory, filename));
        } catch (Exception e) {
            fail("Unexpected error while parsing '" + filename + "': "
                    + e.getMessage());
        }
    }

    public static void testSyntaxError(File directory, String filename) {
        PrintStream prevErr = System.err;

        // Beaver reports syntax error on the standard error.
        // We discard these messages since a syntax error is expected.
        System.setErr(new PrintStream(new ByteArrayOutputStream()));
        try {
            parse(new File(directory, filename));

            fail("syntax is valid, expected syntax error");
        } catch (beaver.Parser.Exception | LangParser.SyntaxError e) {
            // Ok (expected syntax error)!
        } catch (Exception e) {
            fail("IO error while trying to parse '" + filename + "': "
                    + e.getMessage());
        } finally {
            // Restore the system error stream.
            System.setErr(prevErr);
        }
    }


    public static String changeExtension(String filename, String newExtension) {
        int index = filename.lastIndexOf('.');
        if (index != -1) {
            return filename.substring(0, index) + newExtension;
        } else {
            return filename + newExtension;
        }
    }

    public static Collection<Object[]> getTestParameters(File testDirectory, String extension) {
        Collection<Object[]> tests = new LinkedList<Object[]>();
        if (!testDirectory.isDirectory()) {
            throw new Error("Could not find '" + testDirectory + "' directory!");
        }
        for (File f: testDirectory.listFiles()) {
            if (f.isDirectory()) {
                tests.addAll(getTestParameters(f, extension));
            } else if (f.getName().endsWith(extension)) {
                tests.add(new Object[] {f.getName(), f.getAbsolutePath()});
            }
        }
        return tests;
    }
}