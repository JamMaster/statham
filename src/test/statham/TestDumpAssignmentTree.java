package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import statham.ast.model.Program;

import java.io.File;

@RunWith(Parameterized.class)
public class TestDumpAssignmentTree {
    /** Directory where the test input files are stored. */
    private static final File TEST_DIRECTORY_DUMPS = new File("testfiles/ast/dumpassign");

    @Parameterized.Parameter(0)
    public String name;

    @Parameterized.Parameter(1)
    public String filePath;

    @Test
    public void assertAsssignTree() throws Exception {
        Program program = (Program) TestUtil.parse(new File(filePath));

//        TestUtil.compareOutput(program.dumpAssignTree(),
//                new File(TestUtil.changeExtension(filePath, ".out")),
//                new File(TestUtil.changeExtension(filePath, ".expected")));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY_DUMPS, ".in");
    }
}
