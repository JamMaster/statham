package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import statham.ast.model.Program;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;

@RunWith(Parameterized.class)
public class TestStaticAnalysis extends TestBase {
    static {
        TEST_DIRECTORY = new File("testfiles/ast/static_analysis");
        EXTENSION = ".in";
    }

    @Test
    public void assertStaticAnalysis() throws Exception {
        doTest(false);
    }

    //Dont run anything
    @Override
    protected void onNoCompilationErrors(Program program, PrintStream printStream, ByteArrayOutputStream stdout) {
        TestUtil.compareOutput(stdout.toString(),
                new File(TestUtil.changeExtension(filePath, ".out")),
                new File(TestUtil.changeExtension(filePath, ".expected")));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY, EXTENSION);
    }
}
