package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;

@RunWith(Parameterized.class)
public class TestExecution extends TestBase {
    static {
        TEST_DIRECTORY = new File("testfiles/execution");
        EXTENSION = ".stm";
    }

    @Test
    public void assertExecution() throws Exception {
        doTest(true);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY, EXTENSION);
    }
}
