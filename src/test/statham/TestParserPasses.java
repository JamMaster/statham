package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;

@RunWith(Parameterized.class)
public class TestParserPasses {
    private static final File TEST_DIRECTORY_PASSES = new File("testfiles/parser/passes");

    @Parameterized.Parameter(0)
    public String name;

    @Parameterized.Parameter(1)
    public String filePath;

    @Test
    public void assertPass() throws Exception {
        TestUtil.parse(new File(filePath));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY_PASSES, ".in");
    }
}
