package statham;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;

import statham.ast.LangParser;

@RunWith(Parameterized.class)
public class TestParserFails {
    private static final File TEST_DIRECTORY_FAILS = new File("testfiles/parser/fails");

    @Parameterized.Parameter(0)
    public String name;

    @Parameterized.Parameter(1)
    public String filePath;

    @Test(expected = LangParser.SyntaxError.class)
    public void assertFail() throws Exception {
        TestUtil.parse(new File(filePath));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        return TestUtil.getTestParameters(TEST_DIRECTORY_FAILS, ".in");
    }
}
