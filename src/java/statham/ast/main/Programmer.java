package statham.ast.main;

import com.fazecast.jSerialComm.SerialPort;
import org.apache.commons.lang3.ArrayUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Programmer {
    public static final byte MAGIC_INIT = 123;

    private SerialPort arduino;

    public static void main(String[] args) throws IOException {
        try {
            SerialPort port = choosePort();
            Programmer prog = new Programmer(port);
            byte[] binary = readBinaryFile("code.z80");

            System.out.println("Press any key to start programming");
            System.in.read();

            System.out.println("Programming...");
            prog.initProgramming(binary.length);
            prog.writeByteArray(0, binary);

            System.out.println("Programming done.");
            prog.closePort();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }

    private void closePort() {
        arduino.closePort();
    }

    private static byte[] readBinaryFile(String path) throws FileNotFoundException {
        Scanner scan = new Scanner(new FileInputStream(path));
        List<Byte> bytes = new ArrayList<>();

        while (scan.hasNext()) {
            String line = scan.nextLine();

            bytes.add((byte) (Integer.parseInt(line.trim(), 16) & 0xFF));
        }

        System.out.printf("Read %d bytes from file\n", bytes.size());
        return ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
    }

    private static SerialPort choosePort() {
        List<SerialPort> ports = Arrays.asList(SerialPort.getCommPorts());
        if (ports.size() == 0) {
            System.out.println("No USB devices detected");
            System.exit(0);

            return null;
        } else if (ports.size() == 1) {
            System.out.println("Found one device, connecting automatically to " + ports.get(0).getSystemPortName());
            return ports.get(0);
        } else {
            System.out.println("Choose serial port: ");

            for (int i = 0; i < ports.size(); i++) {
                String name = ports.get(i).getSystemPortName();
                System.out.println(i + ": " + name + "\n");
            }
            Scanner scan = new Scanner(System.in);
            try {
                return ports.get(scan.nextInt());
            } catch (InputMismatchException e) {
                System.out.println("You must choose a number between 0 and " + ports.size());
                System.exit(0);
                return null;
            }
        }
    }

    public Programmer(SerialPort port) {
        arduino = port;

        boolean setup = true;
        setup &= arduino.setComPortParameters(9600, 8, 1, 0);
        setup &= arduino.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 0, 0);
        setup &= arduino.openPort();
        if (!setup) {
            throw new IllegalStateException("The setup failed!");
        }
    }

    private void writeByte(int address, byte value) {
        writeByteArray(address, new byte[] {value});
    }

    public void initProgramming(int programSize) {
        arduino.writeBytes(new byte[] {MAGIC_INIT}, 1);
        arduino.writeBytes(shortToBytes(programSize), 2);
        //This will wait for the Arduino to give the go ahead
        byte[] buffer = new byte[1];
        arduino.readBytes(buffer, 1);
        byte response = buffer[0];
        if (response != MAGIC_INIT) {
            throw new IllegalStateException("The echoed magic number incorrect, got " + response);
        }
    }

    private void writeByteArray(int address, byte[] values) {
        arduino.writeBytes(shortToBytes(values.length), 2);
        arduino.writeBytes(shortToBytes(address), 2);
        arduino.writeBytes(values, values.length);
        //We expect the last data echoed back just to be sure
        byte[] buffer = new byte[1];
        arduino.readBytes(buffer, 1);
        byte response = buffer[0];
        if (response != values[values.length - 1]) {
            throw new IllegalStateException("The echoed value was incorrect, expected " + values[values.length - 1] + ", got " + response);
        }
    }

    private byte[] shortToBytes(int value) {
        return new byte[]{
                (byte) ((value & 0xFF00) >>> 8),
                (byte) (value & 0xFF)};
    }
}
