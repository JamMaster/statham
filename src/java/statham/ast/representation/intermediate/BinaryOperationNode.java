package statham.ast.representation.intermediate;

import statham.ast.representation.intermediate.value.Value;

/**
 * Class that represents an operation with two operands
 * @param <T> Type of the first operand
 * @param <U> Type of the second operand
 */
public class BinaryOperationNode<T extends Value, U extends Value> extends StoreNode<T> {
    private U second;
    private Operation operation;

    public enum Operation {
        ADD, SUB, MUL, DIV, MOD, EXP,
        L_SHIFT, R_SHIFT, UR_SHIFT,
        BIT_AND, BIT_OR, BIT_XOR,
        LOGIC_AND, LOGIC_OR,
        LT, GT, LE, GE, EQ, NEQ;
    }

    public BinaryOperationNode(String destName, T first, U second, Operation operation) {
        super(destName, first);
        this.destName = destName;
        this.second = second;
        this.operation = operation;
    }

    public U getSecond() {
        return second;
    }

    public Operation getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        return "ArithmeticNode{" +
                "second=" + second +
                ", operation=" + operation +
                ", destName='" + destName + '\'' +
                ", value=" + value +
                '}';
    }
}
