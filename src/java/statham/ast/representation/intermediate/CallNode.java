package statham.ast.representation.intermediate;

import statham.ast.representation.intermediate.value.Value;

import java.util.List;

public class CallNode extends IntermediateNode {
    protected String name;
    protected List<Value> arguments;

    public CallNode(String name, List<Value> arguments) {
        this.name = name;
        this.arguments = arguments;
    }

    public String getName() {
        return name;
    }

    public List<Value> getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return "CallNode{" +
                "name='" + name + '\'' +
                ", arguments=" + arguments +
                '}';
    }
}
