package statham.ast.representation.intermediate;

public class LabelNode extends IntermediateNode {
    protected String name;

    public LabelNode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + ": ";
    }
}
