package statham.ast.representation.intermediate.value;

public class LiteralValue<T> extends Value {
    private T value;

    public LiteralValue(T initial) {
        this.value = initial;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "LiteralValue{" +
                "value=" + value +
                '}';
    }
}
