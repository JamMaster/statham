package statham.ast.representation.intermediate.value;

public class ArrayValue extends Value {
    private String varName;
    private String indexVarName;

    public ArrayValue(String varName, String indexVarName) {
        this.varName = varName;
        this.indexVarName = indexVarName;
    }

    public String getVarName() {
        return varName;
    }

    public String getIndex() {
        return indexVarName;
    }

    @Override
    public String toString() {
        return "ArrayValue{" +
                "varName='" + varName + '\'' +
                ", indexVarName=" + indexVarName +
                '}';
    }
}
