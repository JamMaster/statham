package statham.ast.representation.intermediate.value;

import statham.ast.representation.AbstractRepresentation;
import statham.ast.representation.intermediate.IntermediateNode;

import java.util.Stack;
import java.util.stream.Collectors;

public class IntermediateRepresentation extends AbstractRepresentation {
    private Stack<IntermediateNode> globals;
    private Stack<IntermediateNode> functions;

    public IntermediateRepresentation() {
        this.globals = new Stack<>();
        this.functions = new Stack<>();
    }

    public Stack<IntermediateNode> getGlobals() {
        return globals;
    }

    public Stack<IntermediateNode> getFunctions() {
        return functions;
    }

    @Override
    public String toString() {
        return "globals=\n" + globals.stream()
                    .map(IntermediateNode::toString)
                    .collect(Collectors.joining("\n")) +
                "\nfunctions=\n" + functions.stream()
                    .map(IntermediateNode::toString)
                    .collect(Collectors.joining("\n"));
    }
}
