package statham.ast.representation.intermediate.value;

public class StructFieldValue extends Value {
    private String varName;
    private String fieldName;

    public StructFieldValue(String varName, String fieldName) {
        this.varName = varName;
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getVarName() {
        return varName;
    }

    @Override
    public String toString() {
        return "StructFieldValue{" +
                "varName='" + varName + '\'' +
                ", fieldName='" + fieldName + '\'' +
                '}';
    }
}
