package statham.ast.representation.intermediate.value;

public class VariableValue extends Value {
    private String varName;

    public VariableValue(String varName) {
        this.varName = varName;
    }

    public String getVarName() {
        return varName;
    }

    @Override
    public String toString() {
        return "VariableValue{" +
                "varName='" + varName + '\'' +
                '}';
    }
}
