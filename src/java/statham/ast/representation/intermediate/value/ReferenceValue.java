package statham.ast.representation.intermediate.value;

public class ReferenceValue extends Value {
    private String varName;

    public ReferenceValue(String varName) {
        this.varName = varName;
    }

    public String getVarName() {
        return varName;
    }

    @Override
    public String toString() {
        return "ReferenceValue{" +
                "varName='" + varName + '\'' +
                '}';
    }
}
