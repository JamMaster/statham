package statham.ast.representation.intermediate.value;

public class FuncReferenceValue extends Value {
    private String funcName;

    public FuncReferenceValue(String funcName) {
        this.funcName = funcName;
    }

    public String getFuncName() {
        return funcName;
    }

    @Override
    public String toString() {
        return "FuncReferenceValue{" +
                "funcName='" + funcName + '\'' +
                '}';
    }
}
