package statham.ast.representation.intermediate;

import statham.ast.representation.intermediate.value.Value;

public class StoreNode<T extends Value> extends IntermediateNode {
    protected String destName;
    protected T value;

    public StoreNode(String destName, T value) {
        this.destName = destName;
        this.value = value;
    }

    public String getDestName() {
        return destName;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "StoreNode{" +
                "destName='" + destName + '\'' +
                ", value=" + value +
                '}';
    }
}
