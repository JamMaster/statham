package statham.ast.representation.intermediate;

import statham.ast.representation.intermediate.value.Value;

/**
 * Class that represents an operation on one operand
 * @param <T> Type of the operand
 */
public class UnaryOperationNode<T extends Value> extends StoreNode<T> {
    private Operation operation;

    public enum Operation {
        PLUS, MINUS,
        BIT_NOT, LOGIC_NOT,
        ADDR_OF,
        PRE_INC, PRE_DEC,
        POST_INC, POST_DEC,
    }

    public UnaryOperationNode(String destName, T first, Operation operation) {
        super(destName, first);
        this.destName = destName;
        this.operation = operation;
    }

    public Operation getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        return "ArithmeticNode{" +
                "operation=" + operation +
                ", destName='" + destName + '\'' +
                ", value=" + value +
                '}';
    }
}
