package statham.ast.representation.intermediate;

import statham.ast.representation.intermediate.value.Value;

import java.util.List;

public class SyscallNode<T extends Value> extends IntermediateNode {
    protected Type call;
    protected List<T> arguments;

    public enum Type {
        ALLOC, FREE;
    }

    public SyscallNode(Type call, List<T> arguments) {
        this.call = call;
        this.arguments = arguments;
    }

    public Type getCall() {
        return call;
    }

    public List<T> getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return "SyscallNode{" +
                "call=" + call +
                ", arguments=" + arguments +
                '}';
    }
}
