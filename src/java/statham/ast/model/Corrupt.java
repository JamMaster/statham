package statham.ast.model;

public class Corrupt extends InnerType<Object> {
    public Corrupt() {
        super(new Object());
    }

    @Override
    public InnerType<Object> deepCopy() {
        return null;
    }

    @Override
    protected Object getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOfType(Type type) {
        return false;
    }

    @Override
    public String toString() {
        return "Corrupt";
    }
}
