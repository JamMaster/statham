package statham.ast.model;

/**
 * Used only for loop control
 */
public class BreakControlVar extends InnerType<Void> {
    public BreakControlVar() {
        super(null);
    }

    @Override
    public InnerType<Void> deepCopy() {
        throw new IllegalArgumentException("This variable should not be used!");
    }

    @Override
    protected Void getDefaultValue() {
        throw new IllegalArgumentException("This variable should not be used!");
    }

    @Override
    public boolean isOfType(Type type) {
        throw new IllegalArgumentException("This variable should not be used!");
    }
}
