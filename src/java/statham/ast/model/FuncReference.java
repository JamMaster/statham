package statham.ast.model;

import java.util.Optional;

public class FuncReference extends InnerType<FuncDecl> {
    private Type type;

    public FuncReference(FuncDecl initial) {
        super(initial);
    }

    public FuncReference(Type type) {
        //Stupid java needs super to be the first call, so need to do it like this
        super(Optional.of(type).map(t -> {
            if (t instanceof FuncType) {
                return ((FuncType) t).getFuncDecl();
            } else {
                throw new IllegalArgumentException("Type given to FuncReference is not a function");
            }
        }).orElseThrow());
    }

    @Override
    public InnerType<FuncDecl> deepCopy() {
        return new FuncReference(value);
    }

    @Override
    protected FuncDecl getDefaultValue() {
        return null; //We set the default value in constructor, not here
    }

    @Override
    public boolean isOfType(Type type) {
        if (type instanceof FuncType) {
            FuncDecl otherDecl = ((FuncType) type).getFuncDecl();

            if (otherDecl.getNumParameter() != value.getNumParameter()) {
                return false;
            }
            if (!otherDecl.getReturnTypeAccess().type().equals(value.getReturnTypeAccess().type())) {
                return false;
            }

            for (int i = 0; i < value.getNumParameter(); i++) {
                Type otherParamType = otherDecl.getParameter(i).getTypeAccess().type();
                Type thisParamType = value.getParameter(i).getTypeAccess().type();

                if (!otherParamType.equals(thisParamType)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return getValue().signature();
    }
}
