package statham.ast.model;

public class Bool extends InnerType<Boolean> {
    public Bool(Boolean initial) {
        super(initial);
    }

    public Bool() {
        super();
    }

    public Bool invert() {
        value = !value;

        return this;
    }

    @Override
    public InnerType<Boolean> deepCopy() {
        return new Bool(value);
    }

    @Override
    protected Boolean getDefaultValue() {
        return false;
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals("boolean");
    }
}
