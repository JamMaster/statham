package statham.ast.model;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;

public abstract class InnerType<T> {
    /**
     * Used to make sure that the references hash is independent of its contents.
     * This is important when references are stored in hashtables, and then the content is
     * changed
     */
    private static int nextReferenceId = 0;

    private final int id;

    protected T value;

    public InnerType(T initial) {
        this.value = initial;
        this.id = nextReferenceId++;
    }

    public InnerType() {
        this.value = getDefaultValue();
        this.id = nextReferenceId++;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static <U> InnerType instantiate(U val) {
        if (val instanceof Boolean) {
            return new Bool((Boolean) val);
        } else if (val instanceof Character) {
            return new Char((Character) val);
        } else if (val instanceof Integer) {
            return new Int((Integer) val);
        } else if (val instanceof Double) {
            return new Dbl((Double) val);
        } else if (val instanceof String) {
            return new Str((String) val);
        } else if (val instanceof ArrayList) {
            return new Array((ArrayList) val);
        } else if (val instanceof Void) {
            return new InnerVoid();
        } else if (val.getClass().isArray()) {
            java.util.ArrayList<InnerType> arr = new ArrayList<>();

            Object[] a;
            if (val instanceof boolean[]) {
                a = ArrayUtils.toObject((boolean[]) val);
            } else if (val instanceof char[]) {
                a = ArrayUtils.toObject((char[]) val);
            } else if (val instanceof int[]) {
                a = ArrayUtils.toObject((int[]) val);
            } else if (val instanceof double[]) {
                a = ArrayUtils.toObject((double[]) val);
            } else {
                if (val.getClass().getComponentType().isPrimitive()) {
                    throw new IllegalArgumentException("InnerType.instantiate for type " + val.getClass().getSimpleName() + " not implemented!");
                } else {
                    a = (Object[]) val;
                }
            }

            for (Object o : a) {
                arr.add(InnerType.instantiate(o));
            }

            return new Array(arr);
        } else if (val instanceof InnerType) {
            return (InnerType) val;
        } else {
            throw new IllegalArgumentException("InnerType.instantiate for type " + val.getClass().getSimpleName() + " not implemented!");
        }
    }

    public static Double wrapIntOrDouble(Object obj) {
        if (obj instanceof Integer) {
            return ((Integer) obj).doubleValue();
        } else if (obj instanceof Double) {
            return (Double) obj;
        } else {
            throw new IllegalArgumentException("wrapIntOrDouble only applicable for Integer and Double!");
        }
    }

    @Override
    public String toString() {
        return value.toString();
    }

    public String toDebugString() {
        return value.toString() + "(" + getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) + ")";
    }

    public abstract InnerType<T> deepCopy();
    protected abstract T getDefaultValue();

    /**
     * Used for runtime comparison between the evaluated result and the correct type. This luxury is allowed
     * only during interpretation, where the stack frame keeps track of variable types
     * @param type The correct type of this expression derived from static analysis
     * @return whether or not this value wrapper is of the specified type of not
     */
    public abstract boolean isOfType(Type type);

    //We need this for arrays
    @Override
    public boolean equals(Object o) {
        if (o instanceof InnerType) {
            InnerType it = (InnerType) o;

            return this.value.equals(it.value);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }
}
