package statham.ast.model;

import java.util.*;

public class StackFrame {
    private Map<String, InnerType<?>> vars;
    private Map<String, InnerType> globals;
    private Set<InnerType> dynamic;

    public StackFrame() {
        vars = new HashMap<>();
        globals = new HashMap<>();
        dynamic = new HashSet<>();
    }

    /**
     * @return true if the variable already existed, false if it was created
     */
    private boolean setVariableValue(String varName, InnerType value, boolean isGlobal) {
        InnerType prevValue = isGlobal ? globals.get(varName) : vars.get(varName);

        //This is mostly to catch implementation mistakes during dev of the language
        if (prevValue != null && !prevValue.getClass().equals(value.getClass())) {
            throw new ClassCastException("The new value is of incorrect type! " +
                    prevValue.getClass().getSimpleName() + " vs " +
                    value.getClass().getSimpleName());
        } else {
            if (prevValue == null) {
                return (isGlobal ? globals.put(varName, value) : vars.put(varName, value)) != null;
            } else {
                prevValue.setValue(value.getValue());

                return true;
            }
        }
    }

    /**
     * Allocates space on the heap and returns a reference to it. If count
     * is larger than 1, it will return a reference to an array of pointers
     * @param count
     * @return
     */
    public Reference allocateDynamic(int count, Type type) {
        if (count <= 0) {
            throw new RuntimeException("Cannot allocate negative or 0 dynamic memory!");
        } else if (count == 1) {
            InnerType slot = type.defaultValue(type);
            dynamic.add(slot);

            return new Reference(slot);
        } else {
            ArrayList<InnerType> list = new ArrayList<>(count);
            for (int i = 0; i < count; i++) {
                InnerType slot = type.defaultValue(type);
                list.add(slot);
            }
            Array array = new Array(list);
            dynamic.add(array);

            return new Reference(array);
        }
    }

    /**
     * Frees dynamic memory referenced by the argument. Throws an exception if the argument passed
     * does not correspond to any memory (most likely wasn't returned by alloc).
     * @param ref Reference or array of references returned from alloc, representing dynamic memory locations
     *            to be freed
     */
    public void freeDynamic(InnerType ref) {
        if (dynamic.contains(ref.getValue())) {
            if (ref instanceof Reference) {
                Reference r = (Reference) ref;
                dynamic.remove(r.getValue());

                r.getValue().setValue(new Corrupt());
            } else {
                throw new IllegalArgumentException("The argument to free must either be a reference or an array of refs!");
            }
        } else {
            throw new IllegalArgumentException("Bad argument to free! No record of given reference was found!");
        }
    }

    /**
     * Used at the end of the functions to check for memory leaks.
     * @return The number of dynamic memory slots that are still allocated
     */
    public int getNumDynamic() {
        return dynamic.size();
    }

    /**
     * Used for passing the global variable map and the dynamic memory down the call stack
     * @param other The outer stack frame
     * @return the new stack frame for easier chaining
     */
    public StackFrame withGlobalsAndDynamicFrom(StackFrame other) {
        this.globals = other.globals;
        this.dynamic = other.dynamic;

        return this;
    }

    /**
     * Sets the value of variable associated with the Access. This is the main way of setting
     * values
     *
     * @param variable Access associated with the variable
     * @param value    The value
     * @return Whether or not the variable already had a value assigned
     */
    public boolean setVariableValue(Access variable, InnerType<?> value) {
        if (variable instanceof IdUse) {
            IdUse use = (IdUse) variable;

            return setVariableValue(use.getTypedID(), value, use.decls().get(0).isGlobal());
        } else if (variable instanceof ArrayAccess) {
            ArrayAccess arrayAccess = (ArrayAccess) variable;

            return setArrayElement(arrayAccess, value);
        } else if (variable instanceof DereferenceExpr) {
            DereferenceExpr derefExpr = (DereferenceExpr) variable;
            ((Reference) derefExpr.getOperand().eval(this)).getValue().setValue(value.getValue());

            return true;
        } else if (variable instanceof StructFieldAccess) {
            StructFieldAccess fieldAccess = (StructFieldAccess) variable;

            Map<String, InnerType> innerMap = fieldAccess.evalToMap(this);
            String fieldName = fieldAccess.getField().getID();

            innerMap.get(fieldName).setValue(value.getValue());

            return true;
        } else {
            throw new IllegalArgumentException("No support for access " + variable.getClass().getSimpleName());
        }
    }

    /**
    * Sets the value of variable associated with the IdDecl. I think its used with
    * initialization during veriable declaration.
    * @param varDecl IdDecl associated with the variable
    * @param value The value
    * @return Whether or not the variable already had a value assigned
    */
    public boolean setVariableValue(IdDecl varDecl, InnerType value) {
        return setVariableValue(varDecl.getTypedID(), value, varDecl.isGlobal());
    }

    /**
     * Removes variables from the stack frame. This is called after
     * we leave a block. You can't use out-of-scope variables
     * due to static analysis, but this cleans up the mess
     *
     * If IdDecl doesn't exist, nothing happens
     * @param decls IdDecls for variables to remove
     */
    public void removeVariables(java.util.List<IdDecl> decls) {
        for (IdDecl decl : decls) {
            vars.remove(decl.getID());
        }
    }

    /**
     * Destroys all of the variables in decls. It assigns Corrupt to values with the purpose
     * of crashing the program if one of them is accessed. This is done after exiting a function,
     * to better simulate corruption of stack variables of a real computer. This will come in handy
     * when using the interpreter to debug programs meant for a computer.
     * @param decls List of declarations of variables to be destroyed
     */
    public void destroyVariables(java.util.List<IdDecl> decls) {
        for (IdDecl decl : decls) {
            InnerType val = vars.get(decl.getID());
            if (val != null) {
                val.setValue(new Corrupt());
            }
        }
    }

    /**
     * Gets value associated with the Access
     * @param variable Must be either an IdUse or ArrayAccess
     * @return Value associated with the Access
     */
    public InnerType getVariableValue(Access variable) {
        if (variable instanceof IdUse) {
            IdUse idUse = (IdUse) variable;
            String varName = idUse.getTypedID();

            return idUse.decls().get(0).isGlobal() ? globals.get(varName) : vars.get(varName);
        } else if (variable instanceof ArrayAccess) {
            ArrayAccess arrayAccess = (ArrayAccess) variable;

            return getArrayElement(arrayAccess);
        } else if (variable instanceof DereferenceExpr) {
            DereferenceExpr derefExpr = (DereferenceExpr) variable;

            return ((Reference) derefExpr.getOperand().eval(this)).getValue();
        } else {
            throw new IllegalArgumentException("No support for access " + variable.getClass().getSimpleName());
        }
    }

    private boolean setArrayElement(ArrayAccess arrayAccess, InnerType value) {
        Array array = (Array) arrayAccess.getExpr().eval(this);
        int index = ((Int) arrayAccess.getIndex().eval(this)).getValue();

        try {
            return array.set(index, value) != null;
        } catch (IndexOutOfBoundsException e) {
            throw new StathamRuntimeError(arrayAccess.getLineNum(), arrayAccess.getColNum(), e);
        }
    }

    private InnerType getArrayElement(ArrayAccess arrayAccess) {
        Array array = (Array) arrayAccess.getExpr().eval(this);
        int index = ((Int) arrayAccess.getIndex().eval(this)).getValue();

        try {
            return array.get(index);
        } catch (IndexOutOfBoundsException e) {
            throw new StathamRuntimeError(arrayAccess.getLineNum(), arrayAccess.getColNum(), e);
        }
    }

    /**
     * Used by predefined methods and functions to access the value
     * @param paramName Name of the parameter
     * @return The value of the parameter passed to the invocation
     */
    public InnerType getParamValue(String paramName) {
        return vars.get(paramName);
    }

    /**
     * Whether or not the current stack frame has a specific param. Used to save time
     * implementing predefined overloaded functions. Simply dont set the param for overloads
     * and then check if we have it or not.
     * @param paramName
     * @return
     */
    public boolean hasParam(String paramName) {
        return vars.containsKey(paramName);
    }

    /**
     * Adds variables to the stack frame based on their order and their name in the
     * function definition
     *
     * @param func Function for which the args are being set
     * @param argExprs List of values for every argument
     * @param oldFrame StackFrame of the function that called the function of which we are
     * setting the arguments of.
     */
    public StackFrame setArguments(FuncDecl func, List argExprs, StackFrame oldFrame) {
        if (func.getNumParameter() != argExprs.getNumChild()) {
            throw new IllegalArgumentException("The number of arguments to function " + func.signature() + " doesn't match the function definition ");
        } else {
            for (int i = 0; i < func.getNumParameter(); i++) {
                ParameterDeclaration param = func.getParameter(i);

                InnerType evalArg = argExprs.getChild(i).eval(oldFrame);
                setVariableValue(param.getIdDecl().getTypedID(), evalArg.deepCopy(), false);
            }

            return this;
        }
    }

    @Override
    public String toString() {
        return "StackFrame{" +
                "vars=" + vars +
                ", globals=" + globals +
                '}';
    }
}