package statham.ast.model;

public class Reference extends InnerType<InnerType> {
    public Reference(InnerType initial) {
        super(initial);
    }

    public Reference() {
        super();
    }

    @Override
    public InnerType<InnerType> deepCopy() {
        return new Reference(value);
    }

    @Override
    protected InnerType getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOfType(Type type) {
        //TODO: This is a bad solution. Can give false positive if the value is null
        return type.getID().equals("ref") &&
                (value == null || value.isOfType(((RefType) type).getTargetType()));
    }

    @Override
    public String toString() {
        return getValue() == null ? "*null" : "*" + getValue().toString();
    }
}
