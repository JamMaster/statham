package statham.ast.model;

public class Char extends InnerType<Character> {
    public Char(Character initial) {
        super(initial);
    }

    public Char() {
        super();
    }

    @Override
    public InnerType<Character> deepCopy() {
        return new Char(value);
    }

    @Override
    protected Character getDefaultValue() {
        return '\0';
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals("char");
    }
}
