package statham.ast.model;

import java.util.ArrayList;
import java.util.StringJoiner;

public class Array extends InnerType<ArrayList<InnerType>> {
    public Array(ArrayList<InnerType> initial) {
        super(initial);
    }

    public Array() {
        super();
    }

    @Override
    public InnerType<ArrayList<InnerType>> deepCopy() {
        ArrayList<InnerType> copy = new ArrayList<>();

        for (InnerType t : value) {
            copy.add(t.deepCopy());
        }

        return new Array(copy);
    }

    @Override
    protected ArrayList<InnerType> getDefaultValue() {
        return new ArrayList<>();
    }

    @Override
    public boolean isOfType(Type type) {
        //TODO: This is a bad solution. It can give a false positive if the array is empty
        return type.getID().equals("array") &&
                (value.size() == 0 || value.get(0) == null || value.get(0).isOfType(((ArrayType) type).getBaseType()));
    }

    /**
     * Sets the value at index to something. References are intact
     * @param index Index at which to put the element
     * @param newVal New value of the element
     * @return Deep copy of the element previously at specified place
     */
    public InnerType set(int index, InnerType newVal) {
        InnerType before = value.get(index).deepCopy();

        value.get(index).setValue(newVal.getValue());

        return before;
    }

    /**
     *
     * Sets the reference at index to newLink. Changes reference
     * @param index Index at which to put the reference
     * @param newLink New reference of the element
     * @return Original value of the element previously at specified place
     */
    public InnerType link(int index, InnerType newLink) {
        InnerType before = value.get(index);

        value.set(index, newLink);

        return before;
    }

    public boolean add(InnerType val) {
        return value.add(val);
    }

    public InnerType get(int index) {
        return value.get(index);
    }

    public int size() {
        return value.size();
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", "[", "]");

        for (InnerType it : value) {
            joiner.add(it.toString());
        }

        return joiner.toString();
    }

    @Override
    public String toDebugString() {
        StringJoiner joiner = new StringJoiner(", ", "[", "]");

        for (InnerType it : value) {
            joiner.add(it.toDebugString());
        }

        return joiner.toString();
    }
}
