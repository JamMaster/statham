package statham.ast.model;

public class Str extends InnerType<String> {
    public Str(String initial) {
        super(initial);
    }

    public Str() {
        super();
    }

    @Override
    public InnerType<String> deepCopy() {
        return new Str(value);
    }

    @Override
    protected String getDefaultValue() {
        return "";
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals("string");
    }
}
