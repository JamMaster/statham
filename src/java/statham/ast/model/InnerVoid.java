package statham.ast.model;

public class InnerVoid extends InnerType<Void> {
    public InnerVoid() {
        super();
    }

    @Override
    public InnerType<Void> deepCopy() {
        return null;
    }

    @Override
    protected Void getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOfType(Type type) {
        return false;
    }

    @Override
    public String toString() {
        return "Void";
    }
}
