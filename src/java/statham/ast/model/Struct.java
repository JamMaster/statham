package statham.ast.model;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Struct extends InnerType<Map<String, InnerType>> {
    private String name;

    public Struct(Map<String, InnerType> initial, String name) {
        super(initial);
        this.name = name;
    }

    public Struct(String name) {
        super();
        this.name = name;
    }

    @Override
    public InnerType<Map<String, InnerType>> deepCopy() {
        Map<String, InnerType> copy = value.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().deepCopy()));

        return new Struct(copy, name);
    }

    @Override
    protected Map<String, InnerType> getDefaultValue() {
        return new HashMap<>();
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals(name);
    }
}
