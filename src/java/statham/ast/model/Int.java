package statham.ast.model;

public class Int extends InnerType<Integer> {
    public Int(Integer initial) {
        super(initial);
    }

    public Int() {
        super();
    }

    @Override
    public InnerType<Integer> deepCopy() {
        return new Int(value);
    }

    @Override
    protected Integer getDefaultValue() {
        return 0;
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals("int");
    }
}
