package statham.ast.model;

public class Dbl extends InnerType<Double> {
    public Dbl(Double initial) {
        super(initial);
    }

    public Dbl() {
        super();
    }

    @Override
    public InnerType<Double> deepCopy() {
        return new Dbl(value);
    }

    @Override
    protected Double getDefaultValue() {
        return 0.0;
    }

    @Override
    public boolean isOfType(Type type) {
        return type.getID().equals("double");
    }
}
