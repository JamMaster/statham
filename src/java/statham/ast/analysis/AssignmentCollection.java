package statham.ast.analysis;

import beaver.Symbol;
import statham.ast.model.Expr;

import java.util.*;

public class AssignmentCollection {
    private Map<String, List<Expr>> collection;

    public static class AssignmentEntry {
        String key;
        List<Expr> value;

        public AssignmentEntry(String key, List<Expr> value) {
            this.key = key;
            this.value = value;
        }
    }

    public AssignmentCollection() {
        this.collection = new LinkedHashMap<>();
    }

    public static AssignmentEntry wrap(String key, Expr value) {
        List<Expr> list = new LinkedList<>();
        list.add(value);

        return wrap(key, list);
    }

    public static AssignmentEntry wrap(String key, List<Expr> value) {
        return new AssignmentEntry(key, value);
    }

    public void append(AssignmentCollection other) {
        if (other != null) {
            for (Map.Entry<String, List<Expr>> entry : other.collection.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public List<Expr> put(AssignmentEntry entry) {
        return put(entry.key, entry.value);
    }

    private List<Expr> put(String key, List<Expr> value) {

        if (!collection.containsKey(key)) {
            collection.put(key, new LinkedList<>());
        }
        List<Expr> list = collection.get(key);
        list.addAll(value);

        return list;
    }

    public List<Expr> getExprs(String key) {
        return collection.get(key);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Map.Entry<String, List<Expr>> entry : collection.entrySet()) {
            builder.append("\t");
            builder.append(entry.getKey() + " {");
            for (Expr e : entry.getValue()) {
                builder.append("\t\t[" + Symbol.getLine(e.getStart()) + "]: " + e.getTokens());
            }
            builder.append("\t}");
        }

        return builder.toString();
    }
}
