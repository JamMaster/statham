%%
%start program;

%token JSON_OBJ JSON_ELEM;

%token IDENTIFIER;
%token INTEGER_LITERAL INTEGER_HEX_LITERAL DOUBLE_LITERAL BOOLEAN_LITERAL;
%token CHAR_LITERAL STRING_LITERAL NULL_LITERAL;
%token LBRACE RBRACE SEMICOLON COMMA;
%token IF ELSE FOR WHILE DO;
%token BOOLEAN CHAR DOUBLE STRING VOID INT;
%token BREAK CONTINUE CAST;
%token RETURN;
%token NEW;
%token STRUCT ARROW FUNC;

%token LPAREN RPAREN DOT LBRACK RBRACK;
%token PLUSPLUS MINUSMINUS NOT COMP;
%token EXP ALLOC FREE;
%token MULT DIV MOD;
%token PLUS MINUS;
%token LSHIFT RSHIFT URSHIFT;
%token LT GT LTEQ GTEQ;
%token EQEQ NOTEQ;
%token AND XOR OR;
%token ANDAND OROR;
%token QUESTION COLON;
%token EQ PLUSEQ MINUSEQ MULTEQ DIVEQ MODEQ ANDEQ;
%token XOREQ OREQ LSHIFTEQ RSHIFTEQ URSHIFTEQ;

program :
	    %empty
  | declaration_list;

declaration :
	        function_declaration
  | struct_declaration
  | variable_declaration SEMICOLON;

function_declaration :
		         type_no_struct    IDENTIFIER LPAREN RPAREN block
  | id_use          IDENTIFIER LPAREN RPAREN block
  | type_no_struct    IDENTIFIER LPAREN formal_parameter_list RPAREN block
  | id_use          IDENTIFIER LPAREN formal_parameter_list RPAREN block
  | VOID                IDENTIFIER LPAREN                         RPAREN block
  | VOID                IDENTIFIER LPAREN formal_parameter_list RPAREN block;

declaration_list :
		     declaration
  | declaration_list declaration;

struct_declaration :
		       STRUCT IDENTIFIER LBRACE RBRACE
  | STRUCT IDENTIFIER LBRACE struct_field_declaration_list RBRACE;

struct_field_declaration :
			     id_use          var_id_decl SEMICOLON
  | type_no_struct    var_id_decl SEMICOLON;

struct_field_declaration_list :
			          struct_field_declaration
  | struct_field_declaration_list struct_field_declaration;

formal_parameter_list :
		          formal_parameter
  | formal_parameter_list COMMA formal_parameter;

formal_parameter :
		     type_no_struct    var_id_decl
  | id_use          var_id_decl;

block :
          LBRACE RBRACE
  | LBRACE block_statements RBRACE;

block_statements :
		     block_statement
  | block_statements block_statement;

block_statement :
		    variable_declaration_statement
  | statement;

literal :
	    INTEGER_LITERAL
  | INTEGER_HEX_LITERAL
  | DOUBLE_LITERAL
  | BOOLEAN_LITERAL
  | CHAR_LITERAL
  | STRING_LITERAL
  | NULL_LITERAL;

type_no_struct_no_func :
		           normal_type
  | reference_type;

type_no_struct :
	           type_no_struct_no_func
  | func_type;

normal_type :
	        numeric_type
  | array_type
  | json_type
  | BOOLEAN
  | STRING
  | CHAR;

type_list :
	      type_no_struct
  | id_use
  | type_list COMMA type_no_struct
  | type_list COMMA id_use;

func_type :
	      FUNC LPAREN RPAREN ARROW type_no_struct
  | FUNC LPAREN RPAREN ARROW id_use
  | FUNC LPAREN type_list RPAREN ARROW id_use
  | FUNC LPAREN type_list RPAREN ARROW type_no_struct
  | FUNC LPAREN RPAREN ARROW VOID
  | FUNC LPAREN type_list RPAREN ARROW VOID;

reference_type :
	           type_no_struct_no_func    MULT
  | LPAREN func_type RPAREN   MULT
  | id_use                   MULT
  | VOID                        MULT;

array_type :
	       type_no_struct_no_func                    LBRACK RBRACK    
  | LPAREN func_type RPAREN                   LBRACK RBRACK    
  | id_use                                   LBRACK RBRACK    ;

dim :
        LBRACK expression RBRACK;

dim_list :
	     dim
  | dim_list dim;

array_creation :
	           NEW type_no_struct_no_func    dim_list    
  | NEW LPAREN func_type RPAREN   dim_list    
  | NEW id_use           dim_list    
  | NEW array_type       LBRACE argument_list RBRACE
  | NEW array_type       LBRACE RBRACE;

struct_literal :
	           NEW id_use LBRACE RBRACE
  | NEW id_use LBRACE struct_field_assign_list RBRACE;

struct_field_assign_list :
			     struct_field_assign
  | struct_field_assign_list COMMA struct_field_assign;

struct_field_assign :
		        id_use COLON expression;

lambda_func :
	        FUNC LPAREN RPAREN ARROW type_no_struct block
  | FUNC LPAREN RPAREN ARROW id_use block
  | FUNC LPAREN formal_parameter_list RPAREN ARROW id_use block
  | FUNC LPAREN formal_parameter_list RPAREN ARROW type_no_struct block
  | FUNC LPAREN RPAREN ARROW VOID block
  | FUNC LPAREN formal_parameter_list RPAREN ARROW VOID block;

json_type :
	      JSON_OBJ
  | JSON_ELEM;

numeric_type :
	         integral_type
  | floating_point_type;

integral_type :
	          INT;

floating_point_type :
		        DOUBLE;

id_use :
           IDENTIFIER;

variable_declaration_statement :
			           variable_declaration SEMICOLON;

variable_declaration :
		         type_no_struct    variable_declarators
  | id_use           variable_declarators;

for_each_variable_declaration :
			          type_no_struct    var_id_decl
  | id_use          var_id_decl;

variable_declarators :
		         variable_declarator
  | variable_declarators COMMA variable_declarator;

variable_declarator :
		        var_id_decl
  | var_id_decl EQ expression;

var_id_decl :
	        IDENTIFIER;

statement :
	      block
  | empty_statement
  | expression_statement
  | do_statement
  | break_statement
  | continue_statement
  | return_statement
  | if_statement
  | while_statement
  | for_statement
  | for_each_statement;

if_statement :
	         IF LPAREN expression RPAREN block
  | IF LPAREN expression RPAREN block ELSE block
  | IF LPAREN expression RPAREN block ELSE if_statement;

empty_statement :
		    SEMICOLON;

expression_statement :
		         statement_expression SEMICOLON;

statement_expression :
		         assignment
  | preincrement_expression
  | predecrement_expression
  | postincrement_expression
  | postdecrement_expression
  | function_invocation
  | method_invocation
  | free_expr;

while_statement :
		    WHILE LPAREN expression RPAREN block;

do_statement :
	         DO block WHILE LPAREN expression RPAREN SEMICOLON;

for_each_statement :
		       FOR LPAREN for_each_variable_declaration COLON expression RPAREN block;

for_statement :
	          FOR LPAREN SEMICOLON SEMICOLON RPAREN block
  | FOR LPAREN SEMICOLON SEMICOLON for_update RPAREN block
  | FOR LPAREN SEMICOLON expression SEMICOLON RPAREN block
  | FOR LPAREN SEMICOLON expression SEMICOLON for_update RPAREN block
  | FOR LPAREN for_init SEMICOLON SEMICOLON RPAREN block
  | FOR LPAREN for_init SEMICOLON SEMICOLON for_update RPAREN block
  | FOR LPAREN for_init SEMICOLON expression SEMICOLON RPAREN block
  | FOR LPAREN for_init SEMICOLON expression SEMICOLON for_update RPAREN block;

for_init :
	     statement_expression_list
  | variable_declaration;

for_update :
	       statement_expression_list;

statement_expression_list :
			      statement_expression
  | statement_expression_list COMMA statement_expression;

break_statement :
		    BREAK SEMICOLON;

continue_statement :
		       CONTINUE SEMICOLON;

return_statement :
		     RETURN SEMICOLON
  | RETURN expression SEMICOLON;

function_invocation :
    postfix_expression LPAREN RPAREN
  | postfix_expression LPAREN argument_list RPAREN;

method_invocation :
    id_use DOT id_use LPAREN RPAREN
  | id_use DOT id_use LPAREN argument_list RPAREN
  | primary_no_new_array_or_struct_or_lambda DOT id_use LPAREN RPAREN
  | primary_no_new_array_or_struct_or_lambda DOT id_use LPAREN argument_list RPAREN

array_access :
	         id_use LBRACK expression RBRACK
  | primary_no_new_array_or_struct_or_lambda LBRACK expression RBRACK;

struct_field_access :
		        id_use DOT id_use
  | primary_no_new_array_or_struct_or_lambda DOT id_use;

alloc_expr :
	       ALLOC LT type_no_struct GT LPAREN expression RPAREN
  | ALLOC LT id_use       GT LPAREN expression RPAREN
  | ALLOC LT type_no_struct GT LPAREN RPAREN
  | ALLOC LT id_use       GT LPAREN RPAREN;

free_expr :
	      FREE LPAREN expression RPAREN;

primary :
	    primary_no_new_array_or_struct_or_lambda 
  | array_creation
  | struct_literal
  | lambda_func;

primary_no_new_array_or_struct_or_lambda :
					     literal
  | LPAREN expression RPAREN
  | function_invocation
  | method_invocation
  | array_access
  | struct_field_access
  | alloc_expr
  | free_expr;

argument_list :
	          expression
  | argument_list COMMA expression;

postfix_expression :
		       primary 
  | id_use    
  | postincrement_expression
  | postdecrement_expression;

postincrement_expression :
			     postfix_expression PLUSPLUS ;

postdecrement_expression :
			     postfix_expression MINUSMINUS ;

preincrement_expression :
			    PLUSPLUS unary_expression;

predecrement_expression :
			    MINUSMINUS unary_expression;

unary_expression :
		     postfix_expression 
  | preincrement_expression
  | predecrement_expression
  | AND cast_expression     
  | MULT cast_expression   
  | PLUS cast_expression    
  | MINUS cast_expression   
  | COMP cast_expression
  | NOT cast_expression;

cast_expression :
		    unary_expression
  | CAST LT type_no_struct_no_func GT LPAREN cast_expression RPAREN ;

exponentiation_expression :
			      cast_expression 
  | exponentiation_expression EXP cast_expression;

multiplicative_expression :
			      exponentiation_expression 
  | multiplicative_expression MULT exponentiation_expression
  | multiplicative_expression DIV exponentiation_expression
  | multiplicative_expression MOD exponentiation_expression;

additive_expression :
		        multiplicative_expression 
  | additive_expression PLUS multiplicative_expression
  | additive_expression MINUS multiplicative_expression;

shift_expression :
		     additive_expression 
  | shift_expression LSHIFT additive_expression
  | shift_expression RSHIFT additive_expression
  | shift_expression URSHIFT additive_expression;

relational_expression :
		          shift_expression
  | relational_expression LT shift_expression
  | relational_expression GT shift_expression
  | relational_expression LTEQ shift_expression
  | relational_expression GTEQ shift_expression;

equality_expression :
		        relational_expression
  | equality_expression EQEQ relational_expression
  | equality_expression NOTEQ relational_expression;

and_expression :
	           equality_expression 
  | and_expression AND equality_expression;

exclusive_or_expression :
			    and_expression 
  | exclusive_or_expression XOR and_expression;

inclusive_or_expression :
			    exclusive_or_expression 
  | inclusive_or_expression OR exclusive_or_expression;

conditional_and_expression :
			       inclusive_or_expression 
  | conditional_and_expression ANDAND inclusive_or_expression;

conditional_or_expression :
			      conditional_and_expression 
  | conditional_or_expression OROR conditional_and_expression;

conditional_expression :
		           conditional_or_expression 
  | conditional_or_expression QUESTION expression COLON conditional_expression;

assignment_expression :
		          conditional_expression
  | assignment;

assignment :
	       unary_expression EQ assignment_expression
  | unary_expression MULTEQ assignment_expression
  | unary_expression DIVEQ assignment_expression
  | unary_expression MODEQ assignment_expression
  | unary_expression PLUSEQ assignment_expression
  | unary_expression MINUSEQ assignment_expression
  | unary_expression LSHIFTEQ assignment_expression
  | unary_expression RSHIFTEQ assignment_expression
  | unary_expression URSHIFTEQ assignment_expression
  | unary_expression ANDEQ assignment_expression
  | unary_expression XOREQ assignment_expression
  | unary_expression OREQ assignment_expression;

expression :  assignment_expression;

%%

