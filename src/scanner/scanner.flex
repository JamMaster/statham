package statham.ast;

import statham.ast.LangParser.Terminals;
import statham.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
    StringBuilder string = new StringBuilder();

    private beaver.Symbol sym(short id) {
        return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
    }

    private beaver.Symbol sym(short id, String value) {
        return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), value);
    }

    private beaver.Symbol sym(short id, char value) {
        return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), value);
    }
%}

LineTerminator = \n|\r|\r\n
InputCharacter = [^\r\n]

WhiteSpace = [ ] | \t | \f | {LineTerminator}
Identifier = [:jletter:][:jletterdigit:]*

DecimalNumeral = [0-9]+
HexNumeral = 0 [xX] [0-9a-fA-F]+
FloatingPointNumeral = [0-9]* \.? [0-9]+

Char = [^\r\n\'\\]
String = [^\r\n\"\\]

Comment = \/\/ {InputCharacter}*

%state STRING, CHARLITERAL

%%

<YYINITIAL> {
    // discard whitespace information
    {WhiteSpace}  { }
    {Comment}     { }

    // token definitions

    {DecimalNumeral}        { return sym(Terminals.INTEGER_LITERAL); }
    {HexNumeral}            { return sym(Terminals.INTEGER_HEX_LITERAL); }
    {FloatingPointNumeral}  { return sym(Terminals.DOUBLE_LITERAL); }

    "true"                  { return sym(Terminals.BOOLEAN_LITERAL); }
    "false"                 { return sym(Terminals.BOOLEAN_LITERAL); }

    "null"                  { return sym(Terminals.NULL_LITERAL); }

    "boolean"         { return sym(Terminals.BOOLEAN); }
    "char"            { return sym(Terminals.CHAR); }
    "double"          { return sym(Terminals.DOUBLE); }
    "int"             { return sym(Terminals.INT); }
    "void"            { return sym(Terminals.VOID); }
    "string"          { return sym(Terminals.STRING); }
    "jsonObj"         { return sym(Terminals.JSON_OBJ); }
    "jsonElem"        { return sym(Terminals.JSON_ELEM); }

    "("   { return sym(Terminals.LPAREN); }
    ")"   { return sym(Terminals.RPAREN); }
    "{"   { return sym(Terminals.LBRACE); }
    "}"   { return sym(Terminals.RBRACE); }
    "["   { return sym(Terminals.LBRACK); }
    "]"   { return sym(Terminals.RBRACK); }
    ","   { return sym(Terminals.COMMA); }
    ";"   { return sym(Terminals.SEMICOLON); }
    "."   { return sym(Terminals.DOT); }
    "->"  { return sym(Terminals.ARROW); }

    "func"            { return sym(Terminals.FUNC); }
    "new"             { return sym(Terminals.NEW); }
    "break"           { return sym(Terminals.BREAK); }
    "continue"        { return sym(Terminals.CONTINUE); }
    "do"              { return sym(Terminals.DO); }
    "else"            { return sym(Terminals.ELSE); }
    "for"             { return sym(Terminals.FOR); }
    "if"              { return sym(Terminals.IF); }
    "return"          { return sym(Terminals.RETURN); }
    "while"           { return sym(Terminals.WHILE); }
    "struct"          { return sym(Terminals.STRUCT); }
    "cast"            { return sym(Terminals.CAST); }
    "alloc"           { return sym(Terminals.ALLOC); }
    "free"            { return sym(Terminals.FREE); }

    "="                            { return sym(Terminals.EQ); }
    ">"                            { return sym(Terminals.GT); }
    "<"                            { return sym(Terminals.LT); }
    "!"                            { return sym(Terminals.NOT); }
    "~"                            { return sym(Terminals.COMP); }
    "?"                            { return sym(Terminals.QUESTION); }
    ":"                            { return sym(Terminals.COLON); }
    "=="                           { return sym(Terminals.EQEQ); }
    "<="                           { return sym(Terminals.LTEQ); }
    ">="                           { return sym(Terminals.GTEQ); }
    "!="                           { return sym(Terminals.NOTEQ); }
    "&&"                           { return sym(Terminals.ANDAND); }
    "||"                           { return sym(Terminals.OROR); }
    "++"                           { return sym(Terminals.PLUSPLUS); }
    "--"                           { return sym(Terminals.MINUSMINUS); }
    "+"                            { return sym(Terminals.PLUS); }
    "-"                            { return sym(Terminals.MINUS); }
    "*"                            { return sym(Terminals.MULT); }
    "^^"                           { return sym(Terminals.EXP); }
    "/"                            { return sym(Terminals.DIV); }
    "&"                            { return sym(Terminals.AND); }
    "|"                            { return sym(Terminals.OR); }
    "^"                            { return sym(Terminals.XOR); }
    "%"                            { return sym(Terminals.MOD); }
    "<<"                           { return sym(Terminals.LSHIFT); }
    ">>"                           { return sym(Terminals.RSHIFT); }
    ">>>"                          { return sym(Terminals.URSHIFT); }
    "+="                           { return sym(Terminals.PLUSEQ); }
    "-="                           { return sym(Terminals.MINUSEQ); }
    "*="                           { return sym(Terminals.MULTEQ); }
    "/="                           { return sym(Terminals.DIVEQ); }
    "&="                           { return sym(Terminals.ANDEQ); }
    "|="                           { return sym(Terminals.OREQ); }
    "^="                           { return sym(Terminals.XOREQ); }
    "%="                           { return sym(Terminals.MODEQ); }
    "<<="                          { return sym(Terminals.LSHIFTEQ); }
    ">>="                          { return sym(Terminals.RSHIFTEQ); }
    ">>>="                         { return sym(Terminals.URSHIFTEQ); }

    /* string literal */
    \"                             { yybegin(STRING); string.setLength(0); }

    /* character literal */
    \'                             { yybegin(CHARLITERAL); }

    {Identifier}      { return sym(Terminals.IDENTIFIER); }
}

<STRING> {
  \"                             { yybegin(YYINITIAL); return sym(Terminals.STRING_LITERAL, string.toString()); }

  {String}+                      { string.append(yytext()); }

  /* escape sequences */
  "\\0"                          { string.append('\0'); }
  "\\b"                          { string.append('\b'); }
  "\\t"                          { string.append('\t'); }
  "\\n"                          { string.append('\n'); }
  "\\f"                          { string.append('\f'); }
  "\\r"                          { string.append('\r'); }
  "\\\""                         { string.append('\"'); }
  "\\'"                          { string.append('\''); }
  "\\\\"                         { string.append('\\'); }

  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {LineTerminator}               { throw new RuntimeException("Unterminated string at end of line"); }
}

<CHARLITERAL> {
  {Char}\'                       { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, String.valueOf(yytext().charAt(0))); }

  /* escape sequences */
  "\\0"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\0");}
  "\\b"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\b");}
  "\\t"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\t");}
  "\\n"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\n");}
  "\\f"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\f");}
  "\\r"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\r");}
  "\\\""\'                       { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\"");}
  "\\'"\'                        { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\'");}
  "\\\\"\'                       { yybegin(YYINITIAL); return sym(Terminals.CHAR_LITERAL, "\\"); }

  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {LineTerminator}               { throw new RuntimeException("Unterminated character literal at end of line"); }
}

<<EOF>>           { return sym(Terminals.EOF); }

[^]               { throw new SyntaxError("Illegal character <"+yytext()+">"); }
